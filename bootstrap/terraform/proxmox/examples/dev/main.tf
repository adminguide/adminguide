terraform {
    required_providers {
        ansible = { 
            source = "ansible/ansible"
            version = "1.1.0"
        }
        proxmox = {
            source = "telmate/proxmox"
            version = "2.9.14"
        }
    }   
}

terraform {
    backend "local" {
        path = "/root/terraform.tfstate"
    }
}

variable test {
    type = list
    default = ["vm1", "vm2"]
}

# настройки провайдера proxmox
provider "proxmox" {
    # ссылка на апи хоста через который будут передаваться команды проксмоксу
    pm_api_url = "https://192.168.88.200:8006/api2/json"

    # апи токен пользователя под которым будут производиться манипуляции
    pm_api_token_id = "adminguide_tf@pam!tf_token"

    # секрет сгенерированный при создании токена, в открытом виде его лучше не хранить
    pm_api_token_secret = "afa76887-a0f6-4563-96f9-02568ffd99be"

    # настройки для работы через самоподписанный сертификат
    pm_tls_insecure = true
    pm_debug = true
}

# resource is formatted to be "[type]" "[entity_name]" so in this case
# we are looking to create a proxmox_vm_qemu entity named test_server

variable "vms" {
  type = list(object({
    name = string
  }))
  default = [
    { name = "vm1"},
  ]
}

locals {
  flat_vms = {
    for vm in var.vms :
    vm.name => vm
  }
}

module "prox_vm" {
    source = "./modules/test_vm"
    #environment  = "acceptance"
}