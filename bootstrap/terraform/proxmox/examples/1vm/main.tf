terraform {
    required_providers {
        ansible = { 
            source = "ansible/ansible"
            version = "1.1.0"
        }
        proxmox = {
            source = "telmate/proxmox"
            version = "2.9.14"
        }
    }   
    backend "local" {
        path = "/root/terraform.tfstate"
    }
  }

# настройки провайдера proxmox
provider "proxmox" {
    # ссылка на апи хоста через который будут передаваться команды проксмоксу
    pm_api_url = "https://192.168.88.200:8006/api2/json"

    # апи токен пользователя под которым будут производиться манипуляции
    pm_api_token_id = "adminguide_tf@pam!tf_token"

    # секрет сгенерированный при создании токена, в открытом виде его лучше не хранить
    pm_api_token_secret = "afa76887-a0f6-4563-96f9-02568ffd99be"

    # настройки для работы через самоподписанный сертификат
    pm_tls_insecure = true
    pm_debug = true
}

# resource is formatted to be "[type]" "[entity_name]" so in this case
# we are looking to create a proxmox_vm_qemu entity named test_server

# описание одной создаваемой вм типа proxmox_vm_qemu с именем docker-node
resource "proxmox_vm_qemu" "test_vm" {
    name = "ag-test-vm01" # Имя которое ВМ получит в проксмоксе
    
    target_node = "ag-proxmox-ve01" # имя хоста проксмокса на котором будет создана вм

    # vmid = 0 # VMID который будет задан новой ВМ. Если 0 - будет использован ближайший свободный

    desc = "AdminGuide.Ru test VM" # дескрипшен который отобразится в Notes в проксмоксе
    
    clone = "u22s" # имя кланируемого темплейта, который уже имеется на целевом проксмоксе

    # full_clone = false #вместо полноценного клона создать linked

    # hastate = "enabled" # включить режи HA для ВМ

    # onboot = true # Запускать ВМ вместе с хостом проксмокса

    # oncreate = true # Запустить ВМ после создания

    # boot = "c" # загружаться только с disk. 

    bootdisk = "scsi0" # Диск с какого именно надо загружаться

    agent = 1 # Использовать qemu guest agent

    
    os_type = "cloud-init"
    cores = 2
    sockets = 1
    cpu = "host"
    memory = 2048
    scsihw = "virtio-scsi-pci"
    
    tags = "test1,service1"
    # создаваемый диск
    disk {
        slot = 0
        size = "9G"
        type = "scsi"
        storage = "local-lvm" # хранилище где он будет создан
        iothread = 0
    }

    # настройка сети для одного интерфейса
    network {
        model = "virtio"
        bridge = "vmbr0"
    }
    
    lifecycle {
        ignore_changes = [
        network,
        ]
    }

    # задаём айпи адрес который будет у интерфейса
    #ipconfig0 = "ip=192.168.88.51/24,gw=192.168.88.1"
    ipconfig0 = "ip=dhcp"

    # provisioner "local-exec" {
    #     command = "ansible-playbook -i ${path.module}/inventory.yml ${path.module}/test_pb.yml"
    # }
}
#
resource "proxmox_vm_qemu" "test_vm2" {
    name = "ag-test-vm02" # Имя которое ВМ получит в проксмоксе
    
    target_node = "ag-proxmox-ve01" # имя хоста проксмокса на котором будет создана вм

    # vmid = 0 # VMID который будет задан новой ВМ. Если 0 - будет использован ближайший свободный

    desc = "AdminGuide.Ru test VM" # дескрипшен который отобразится в Notes в проксмоксе
    
    clone = "u22s" # имя кланируемого темплейта, который уже имеется на целевом проксмоксе

    # full_clone = false #вместо полноценного клона создать linked

    # hastate = "enabled" # включить режи HA для ВМ

    # onboot = true # Запускать ВМ вместе с хостом проксмокса

    # oncreate = true # Запустить ВМ после создания

    # boot = "c" # загружаться только с disk. 

    bootdisk = "scsi0" # Диск с какого именно надо загружаться

    agent = 1 # Использовать qemu guest agent

    
    os_type = "cloud-init"
    cores = 2
    sockets = 1
    cpu = "host"
    memory = 2048
    scsihw = "virtio-scsi-pci"
    
    tags = "test1,service1"
    # создаваемый диск
    disk {
        slot = 0
        size = "9G"
        type = "scsi"
        storage = "local-lvm" # хранилище где он будет создан
        iothread = 0
    }

    # настройка сети для одного интерфейса
    network {
        model = "virtio"
        bridge = "vmbr0"
    }
    
    lifecycle {
        ignore_changes = [
        network,
        ]
    }

    # задаём айпи адрес который будет у интерфейса
    #ipconfig0 = "ip=192.168.88.51/24,gw=192.168.88.1"
    ipconfig0 = "ip=dhcp"

    # provisioner "local-exec" {
    #     command = "ansible-playbook -i ${path.module}/inventory.yml ${path.module}/test_pb.yml"
    # }
}